using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using Xunit;

namespace SampleApp.UiTests
{
    public class SampleAppTest
    {
        private static IWebDriver CreateWebDriver()
        {
            var seleniumHost = Environment.GetEnvironmentVariable("SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR");
            if (string.IsNullOrEmpty(seleniumHost))
            {
                var options = new ChromeOptions();
                var driver = new ChromeDriver(options) {Url = "http://localhost:5000"};
                return driver;
            }

            return new RemoteWebDriver(new Uri($"http://{seleniumHost}:4444/wd/hub"), new ChromeOptions())
            {
                Url = "http://172.17.0.3"
            };
        }

        [Fact]
        public void WelcomeMessage()
        {
            using (var driver = CreateWebDriver())
            {
                var actual = driver.FindElement(By.TagName("h1")).Text;

                Assert.Equal("Welcome to the GitLab CI / CD world!", actual);
            }
        }
    }
}